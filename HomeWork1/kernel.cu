#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <device_functions.h>

int cpu_reduce(int *data, unsigned int n) {
	int res = 0;
	for (int i = 0; i < n; ++i)
		res += data[i];
	return res;
}

__global__ void reduce(int *g_idata, int *g_odata) {
	extern __shared__ int sdata[];
	unsigned int tid = threadIdx.x;
	sdata[tid] = g_idata[blockIdx.x * blockDim.x + threadIdx.x];
	__syncthreads();
	for (unsigned int s = blockDim.x / 2; s>0; s >>= 1) { 
		if (tid < s) { 
			sdata[tid] += sdata[tid + s];
		} 
		__syncthreads();
	}

	if (tid == 0) g_odata[blockIdx.x] = sdata[0];
}

int main(int argc, char **argv) {
	float timerValueGPU;
	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);

	int dev = 0;
	cudaDeviceProp prop;
	cudaGetDeviceProperties(&prop, dev);
	int size = 1000000;
	printf("device %s - %d elements\r\n", prop.name, size);
	cudaSetDevice(dev);
	if (argc > 1)
		size = atoi(argv[1]);
	int blockSize = 512;
	const dim3 block(blockSize, 1);
	const dim3 grid((size + block.x - 1) / block.x, 1);
	size_t bytes = size * sizeof(int);
	int *h_idata = (int *)malloc(bytes);
	int *h_odata = (int *)malloc(grid.x * sizeof(int));

	for (int i = 0; i < size; ++i)
		h_idata[i] = (int)(rand() & 0xFF);

	int *d_idata;
	int *d_odata;
	cudaMalloc((void **)&d_idata, bytes);
	cudaMalloc((void **)&d_odata, bytes);

	clock_t startc, end;
	double cpu_time_used;

	startc = clock();

	int cpu_sum = cpu_reduce(h_idata, size);

	end = clock();
	cpu_time_used = ((double)(end - startc)) / CLOCKS_PER_SEC;
	cpu_time_used *= 1000;

	printf("cpu reduce elapsed %f ms. cpu_sum = %d\n", cpu_time_used, cpu_sum);

	cudaMemcpy(d_idata, h_idata, bytes, cudaMemcpyHostToDevice);
	cudaDeviceSynchronize();

	cudaEventRecord(start, 0);
	reduce<<<grid, block, blockSize*sizeof(int)>>>(d_idata, d_odata);
	cudaDeviceSynchronize();
	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&timerValueGPU, start, stop);

	cudaMemcpy(h_odata, d_odata, grid.x * sizeof(int), cudaMemcpyDeviceToHost);
	cudaDeviceSynchronize();

	int gpu_sum = 0;
	for (int i = 0; i < grid.x; ++i) gpu_sum += h_odata[i];
	printf("gpu reduce1 elapsed %f ms. gpu_sum = %d", timerValueGPU, gpu_sum, grid.x, block.x);
	cudaFree(d_idata);
	cudaFree(d_odata);
	cudaDeviceReset();
	free(h_odata);
	free(h_idata);
	return 0;
}

